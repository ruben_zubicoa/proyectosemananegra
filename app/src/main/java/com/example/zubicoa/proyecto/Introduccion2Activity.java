package com.example.zubicoa.proyecto;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;



public class Introduccion2Activity extends Activity {


    TextView texto1;
    Button btn;
    static final int READ_BLOCK_SIZE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_introduccion2);


        texto1 = (TextView) findViewById(R.id.texto);



texto1.setText(((MyApplication) this.getApplication()).devolverIntroduccion2Activity().toString());


        ((MyApplication) this.getApplication()).inicializarI();


        final String i = ((MyApplication)this.getApplication()).getI().toString();


        btn = (Button) findViewById(R.id.btn);
        texto1.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                try {
                    FileOutputStream fileout=openFileOutput("nivel.txt", MODE_PRIVATE);
                    OutputStreamWriter outputWriter=new OutputStreamWriter(fileout);
                    outputWriter.write(i);
                    outputWriter.close();

                    //display file saved message
                    //Toast.makeText(getBaseContext(), "File saved successfully!",
                      //      Toast.LENGTH_SHORT).show();

                    texto1.setText("archivo guardado");

                } catch (Exception e) {
                    e.printStackTrace();
                }

              Intent intent = new Intent(Introduccion2Activity.this, PistasActivity.class);
               startActivity(intent);
            }
        });

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    FileInputStream fileIn=openFileInput("nivel.txt");
                    InputStreamReader InputRead= new InputStreamReader(fileIn);

                    char[] inputBuffer= new char[READ_BLOCK_SIZE];
                    String s="";
                    int charRead;

                    while ((charRead=InputRead.read(inputBuffer))>0) {
                        // char to string conversion
                        String readstring=String.copyValueOf(inputBuffer,0,charRead);
                        s +=readstring;
                    }
                    InputRead.close();
                    texto1.setText(s);
                    //Toast.makeText(getBaseContext(), s,Toast.LENGTH_SHORT).show();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }
}
