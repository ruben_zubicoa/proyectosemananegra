package com.example.zubicoa.proyecto;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import static com.example.zubicoa.proyecto.Introduccion2Activity.READ_BLOCK_SIZE;

public class PistasActivity extends Activity {

    List<String> pistas = new ArrayList<>();

    TextView titulo;
    TextView texto;
    double latitud;
    double longitud;

    Double tvLatitud, tvLongitud;
    LocationManager locManager;
    Location loc;
    ProgressBar barra;

    String fichero;
    int num;
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {

pistas = ((MyApplication) this.getApplication()).devolverPistas();


        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pistas);

        barra = (ProgressBar) findViewById(R.id.Barra);

        barra.setMax(1000);
        barra.setMin(0);
        barra.setMinimumHeight(50);
        barra.setMinimumWidth(200);


        ((MyApplication)  this.getApplication()).setContadorActivity(1);


        try {
            FileOutputStream fileout=openFileOutput("activity.txt", MODE_PRIVATE);
            OutputStreamWriter outputWriter=new OutputStreamWriter(fileout);
            outputWriter.write( ((MyApplication) this.getApplication()).getContadorActivity());
            outputWriter.close();



        } catch (Exception e) {
            e.printStackTrace();
        }
















        try {
            FileInputStream fileIn = openFileInput("nivel.txt");
            InputStreamReader InputRead = new InputStreamReader(fileIn);

            char[] inputBuffer = new char[READ_BLOCK_SIZE];
            String s = "";
            int charRead;

            while ((charRead = InputRead.read(inputBuffer)) > 0) {
                // char to string conversion
                String readstring = String.copyValueOf(inputBuffer, 0, charRead);
                s += readstring;
                num = Integer.parseInt(s);
            }
            InputRead.close();
            fichero = s;
            //Toast.makeText(getBaseContext(), s,Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
        }



        int nivel;


if (num > 0){
          nivel = num;

    titulo = (TextView) findViewById(R.id.titulo);
    titulo.setText("Pista : " + (nivel + 1));


    texto = (TextView) findViewById(R.id.texto);
    texto.setText(pistas.get(nivel));
    ((MyApplication) this.getApplication()).setI(nivel);
}else{


    nivel= ((MyApplication) this.getApplication()).getI().intValue();
    titulo = (TextView) findViewById(R.id.titulo);
    titulo.setText("Pista : " + (nivel + 1));


    texto = (TextView) findViewById(R.id.texto);
    texto.setText(pistas.get(nivel));
}











        Button btn = (Button) findViewById(R.id.btn);

        btn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PistasActivity.this, AcertijosActivity.class);

                startActivity(intent);
            }
        });





























        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
        } else {
            locationStart();
        }

    }
























    private void locationStart() {
        LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        PistasActivity.Localizacion Local = new PistasActivity.Localizacion();
        Local.setMainActivity(this);
        final boolean gpsEnabled = mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!gpsEnabled) {
            Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(settingsIntent);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
            return;
        }
        mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, (LocationListener) Local);
        mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) Local);

        //mensaje1.setText("Localizacion agregada");
        //mensaje2.setText("");
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 1000) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                locationStart();
                return;
            }
        }
    }

    /* Aqui empieza la Clase Localizacion */
    public class Localizacion implements LocationListener {
        PistasActivity mainActivity;

        public PistasActivity getMainActivity() {
            return mainActivity;
        }

        public void setMainActivity(PistasActivity mainActivity) {
            this.mainActivity = mainActivity;
        }

        @Override
        public void onLocationChanged(Location loc) {
            // Este metodo se ejecuta cada vez que el GPS recibe nuevas coordenadas
            // debido a la deteccion de un cambio de ubicacion

            loc.getLatitude();
            loc.getLongitude();

            latitud = loc.getLatitude();
            longitud = loc.getLongitude();

            Location locationA = new Location("punto A");

            locationA.setLatitude(loc.getLatitude());
            locationA.setLongitude(loc.getLongitude());

            Location locationB = new Location("punto B");

            locationB.setLatitude(42.813537);
            locationB.setLongitude(-1.664832);

            float distance = locationA.distanceTo(locationB);
            String mensaje = distance + " metros";






            if (distance < 1000){
                if (distance <= 250){
                    barra.getProgressDrawable().setColorFilter(Color.GREEN, PorterDuff.Mode.SRC_IN);
                }else if(distance <= 500 && distance > 250){
                    barra.getProgressDrawable().setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_IN);
                }else if(distance <= 750 && distance > 500){
                    barra.getProgressDrawable().setColorFilter(Color.rgb(255,117,020), PorterDuff.Mode.SRC_IN);
                }else if(distance < 1000 && distance > 750){
                    barra.getProgressDrawable().setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);
                }

                barra.setProgress(1000 - Math.round(distance));
            }else{
                barra.setProgress(10);
                barra.getProgressDrawable().setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);
            }


            titulo.setText(Math.round(distance)+ "");
           // Toast.makeText(this.getMainActivity(), mensaje, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onProviderDisabled(String provider) {
            // Este metodo se ejecuta cuando el GPS es desactivado
            Toast.makeText(this.getMainActivity(), "GPS Desactivado", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onProviderEnabled(String provider) {
            // Este metodo se ejecuta cuando el GPS es activado
            Toast.makeText(this.getMainActivity(), "GPS Activado", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            switch (status) {
                case LocationProvider.AVAILABLE:
                    Log.d("debug", "LocationProvider.AVAILABLE");
                    break;
                case LocationProvider.OUT_OF_SERVICE:
                    Log.d("debug", "LocationProvider.OUT_OF_SERVICE");
                    break;
                case LocationProvider.TEMPORARILY_UNAVAILABLE:
                    Log.d("debug", "LocationProvider.TEMPORARILY_UNAVAILABLE");
                    break;
            }
        }


    }
}
