package com.example.zubicoa.proyecto;

import android.app.Application;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zubicoa on 16/10/2017.
 */

public class MyApplication extends Application {

    private Integer i;
    private Integer contadorActivity = 0;
    private Integer video = 0;
    public boolean idioma = true;

    private List<String> pistas = new ArrayList<>();
    private List<String> acertijos = new ArrayList<>();

    public Integer getContadorActivity() {
        return contadorActivity;
    }

    public void setContadorActivity(Integer contadorActivity) {
        this.contadorActivity = contadorActivity;
    }

    private List<String> aciertos = new ArrayList<>();


    public Integer getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public Integer getVideo() {
        return video;
    }

    public void setVideo(Integer video) {
        this.video = video;
    }

    public void sumarI(){
        this.i++;
        this.contadorActivity = 0;
    }

    public void inicializarI(){
        this.i = 0;

    }

    public  void sumarVideo(){
        this.video++;
    }



    public void sumarActivity(){
        this.contadorActivity++;
    }

    public String devolverMain(){

        if (this.idioma == true){
            return "hola";
        }else{
            return "kaixo";
        }
    }

    public List<String> devolverPistas(){

        if (this.idioma == true){
            pistas.add("En las calles de pamplona hay varias estatuas dedicadas a escritores, pero solo en una de ellas el autor representado esta escribiendo \n \n " + "situese frente a ella");
            pistas.add("Busque el edificio donde estuvo instalado el primer instituto de Segunda Enseñanza de Navarra.\n En él estudió un famoso autor de la llamada \n Generacion del 98. \n \n coloquese frente a su puerta");
            pistas.add("localice el primer edificio modernista que se construyó en la ciudad. Cerca de él hay un comercio con el nombre de un famoso héroe de la mitologia griega \n \n Enfréntese a su estrella.");
            pistas.add("Encuentre el lugar donde, durante varios siglos, estuvo situada la carcel de Pamplona. \n Muy cerca hay un edificio gótico que sigue albergando la misma institucion que se instaló allí \n en el siglo XVI \n \n sitúese bajo su dintel");
            pistas.add("Si quiere liberar al señor belzunce, acuda al sitio en el que se alzava la plaza de toros mas antigua que tuvo Pamplona. \n Una vez allí, deberá encontrar la única columna rematada por un capitel jónico que existe en el lugar \n \n ¡Tóquela!");
            return  pistas;
        }else{
            return  pistas;
        }

    }


    public List<String> devolverAcertijos(){
        if(this.idioma == true){
            acertijos.add("Una mañana fui a retirar dinero del cajero automatico y me fue imposible, de modo que, para solucionar el problema, llame al Servicio de Atencion al Cliente. Esta fue la conversacion: \n \n " +
                    "-Yo: buenos dias, no se que sucede, mi clave es distinta y, por alguna razon, no puedo sacar efectivo. \n \n" +
                    "-Servicio de Atencion al Cliente: Un momento, por favor, no se retire \n \n" +
                    "Tras unos segundos, en los que escuche a mi interlocutor teclear en el ordenador, la voz regreso: \n \n" +
                    "-Servicio de Atencion al Cliente:Disculpe la espera. A partir de este instante, su clave ya es otra. ¿Desea algo mas? \n \n" +
                    "-Yo: no, muchas gracias. \n \n" +
                    "Al teclear la nueva clave, pude al fin sacar dinero. ¿Cual era la antigua y cual es la nueva?");
            acertijos.add("Si quiere tener acceso a una nueva pista, rellene el siguiente cuadro teniendo en cuenta las intstrucciones: \n \n" +
                    "a) Los numeros 3, 6, 8 estan en la horizontal superior \n \n" +
                    "b) Los numeros 5, 7, 9 estan en la horizontal inferior \n \n" +
                    "c) Los numeros 1, 2, 3, 6, 7, 9 no estan en la vertical izquierda. \n \n" +
                    "d) Los numeros 1, 3, 4, 5, 8, 9 no estan en la vertical derecha");
            acertijos.add("Anteayer, Juan tenia 15 años; el año que viene cumplira 18 \n ¿Que dia es hoy?");
            acertijos.add("Veamos cuanto sabe usted de numeros, inspector. \n \n " +
                    "La clave para acceder a la siguiente pista es una cifra de seis digitos que debe reunir las siguientes condiciones. \n \n" +
                    "a)Ninguno de ellos es impar. \n \n" +
                    "b)El primero es un terco del quinto y la mitad del tercero. \n \n" +
                    "c)El segundo es el menor de todos. \n \n" +
                    "d)El ultimo es la diferencia entre el cuarto y el quinto.");
            acertijos.add("Un grupo de policias quiere infiltar a uno de sus agentes en un club clandestino; para ello , primero deben averiguar la contraseña que les dara acceso al interior, de modo que deciden vigilar el lugar. \n" +
                    "A los diez minutos, un tipo se acerca a la puerta y llama. Una voz pronuncia: 18, a lo que el responde: 9, tras lo que le dejan pasar. Al cabo de un rato, otro hombre aparece y llama. En esta ocasion, la voz señala: 8, a los que el recien llegado contesta: 4 para que le franqueen la entrada, y apenas unpar de minutos despues, un nuevo visitante responde 7 al 14 planteado desde el interior. \n \n" +
                    "Los policias creen haber dado al fin con la clave, de modo que deciden enviar al agente de incognito. Al llamar, la voz dice: 0, y el contesta: 0. Pero la puerta no solo no se abre, sino que le disparan a traves de ella \n \n " +
                    "Convencidos de que el sistema de claves que han diseñado es el correcto, los policias deciden intentarlo una vez mas. otro agente se acerca despacio y , tras golpear con los nudillos, escucha: 6, a lo que responde 3 con identico resultado que su compañero \n \n \n " +
                    "¿Cuales eran las claves correctas que tenian que haber pronunciado los policias para entrar?");
            acertijos.add("Una mañana fui a retirar dinero del cajero automatico y me fue imposible, de modo que, para solucionar el problema, llame al Servicio de Atencion al Cliente. Esta fue la conversacion: \n \n " +
                    "-Yo: buenos dias, no se que sucede, mi clave es distinta y, por alguna razon, no puedo sacar efectivo. \n \n" +
                    "-Servicio de Atencion al Cliente: Un momento, por favor, no se retire \n \n" +
                    "Tras unos segundos, en los que escuche a mi interlocutor teclear en el ordenador, la voz regreso: \n \n" +
                    "-Servicio de Atencion al Cliente:Disculpe la espera. A partir de este instante, su clave ya es otra. ¿Desea algo mas? \n \n" +
                    "-Yo: no, muchas gracias. \n \n" +
                    "Al teclear la nueva clave, pude al fin sacar dinero. ¿Cual era la antigua y cual es la nueva?");
            acertijos.add("Si quiere tener acceso a una nueva pista, rellene el siguiente cuadro teniendo en cuenta las intstrucciones: \n \n" +
                    "a) Los numeros 3, 6, 8 estan en la horizontal superior \n \n" +
                    "b) Los numeros 5, 7, 9 estan en la horizontal inferior \n \n" +
                    "c) Los numeros 1, 2, 3, 6, 7, 9 no estan en la vertical izquierda. \n \n" +
                    "d) Los numeros 1, 3, 4, 5, 8, 9 no estan en la vertical derecha");
            acertijos.add("Anteayer, Juan tenia 15 años; el año que viene cumplira 18 \n ¿Que dia es hoy?");
            acertijos.add("Veamos cuanto sabe usted de numeros, inspector. \n \n " +
                    "La clave para acceder a la siguiente pista es una cifra de seis digitos que debe reunir las siguientes condiciones. \n \n" +
                    "a)Ninguno de ellos es impar. \n \n" +
                    "b)El primero es un terco del quinto y la mitad del tercero. \n \n" +
                    "c)El segundo es el menor de todos. \n \n" +
                    "d)El ultimo es la diferencia entre el cuarto y el quinto.");
            acertijos.add("Un grupo de policias quiere infiltar a uno de sus agentes en un club clandestino; para ello , primero deben averiguar la contraseña que les dara acceso al interior, de modo que deciden vigilar el lugar. \n" +
                    "A los diez minutos, un tipo se acerca a la puerta y llama. Una voz pronuncia: 18, a lo que el responde: 9, tras lo que le dejan pasar. Al cabo de un rato, otro hombre aparece y llama. En esta ocasion, la voz señala: 8, a los que el recien llegado contesta: 4 para que le franqueen la entrada, y apenas unpar de minutos despues, un nuevo visitante responde 7 al 14 planteado desde el interior. \n \n" +
                    "Los policias creen haber dado al fin con la clave, de modo que deciden enviar al agente de incognito. Al llamar, la voz dice: 0, y el contesta: 0. Pero la puerta no solo no se abre, sino que le disparan a traves de ella \n \n " +
                    "Convencidos de que el sistema de claves que han diseñado es el correcto, los policias deciden intentarlo una vez mas. otro agente se acerca despacio y , tras golpear con los nudillos, escucha: 6, a lo que responde 3 con identico resultado que su compañero \n \n \n " +
                    "¿Cuales eran las claves correctas que tenian que haber pronunciado los policias para entrar?");

            return  acertijos;

        }else{
            return  acertijos;
        }

    }



    public List<String> devolverAciertos(){
        if (this.idioma == true){
            aciertos.add("Muy bien. \n" +
                    "Ha acertado, inspector. \n \n" +
                    "Pero no crra que todo va a ser tan facil... \n" +
                    "La siguiente pista le llevara a otro lugar de Pamplona relacionado con un escritor \n \n" +
                    "El tiempo corre...");
            aciertos.add("¡Enhorabuena! \n \n Esta usted un poco mas cerca de tu destino.");
            aciertos.add("¡Es correcto! \n \n" +
                    "Se ha ganado el derecho a una nueva pista.");
            aciertos.add("Muy bien \n \n" +
                    "Empiezo a creer que es usted un digno rival. \n Solo una nueva pista y un ultimo acertijo nos separan ya...");
            return aciertos;
        }else{
            return  aciertos;
        }
    }


    public String devolverHolaActivity(){
        if (this.idioma == true){
            return "Hola. \n" +
                    "Los escritores de novela negra de Navarra te desafian \n \n" +
                    "Bienvenido al reto";
        }else{
            return "Aupa.\n" +
                    "Desafioka ari zaizkizu nobela beltzeko idazle nafarrak.\n \n" +
                    "Ongi etorri erronkara.";
        }
    }



    public  String devolverIntroduccionActivity(){
        if (this.idioma == true){
            return "Pamplona. \n" +
                    "Enero, 2018, 9:00h. \n \n \n" +
                    "Cuando el inspector entro en su despacho, observo un paquete sobre su mesa. Un agente le indico que lo habia traido un mensajero  primera hora y que el objeto habia pasado los preceptivos controles de seguridad, de modo que cubrio el respaldo de su silla con la chaqueta, se sento y rasgo el papel que lo envolvia. \n \n" +
                    "Frente a el tenia una pequeña de madera, del tamaño de las que suelen contener un reloj. \n \n" +
                    "Cuando la abrio y observo el dedo cortado en el interior, penso que se trataba de una broma. Era demasiada coincidencia que aquel paquete le hubiera llegado en plena semana del famoso encuentro de novela negra que se celebraba en la ciudad. Pero al recibir la llamada del director de festival denunciado que el famoso escritor de best seller Martin Belzunce habia desaparecido sin dejar rastro, supo que el asunto era serio. \n \n" +
                    "Junto al macabro contenido, habia un lapiz de memoria. El inspector lo introdujo en el ordenador y pulso el PLAY... ";

        }else{
            return "Iruña. \n" +
                    "Urtarrila, 2018, 9:00ak.\n\n\n" +
                    "Bere bulegoan sartzean, inspektoreak pakete bat ikusi zuen mahai gainean. Agente batek azaldu zion mezulari batek ekarria zuela goiz-goizean, eta behar diren segurtasun kontrol guztiak pasatuak zituela; beraz, inspektoreak, jakarekin bere aulkiaren bizkarra estalita, eseri eta urratu zuen paketearen paperezko bilgarria.\n\n" +
                    "Parean zurezko kutxa txiki bat zuen, barrenean erloju bat eduki ohi dutenen tamainakoa.\n\n" +
                    "Broma bat zela uste izan zuen ireki zuelarik eta barnean hatz moztua ikusi zuelarik. Kasualitate handiegia zen pakete hori ailegatzea noiz eta hirian eleberri beltzaren gaineko topaketa famatua gauzatzen ari zen astean. Baina afera serioa zela jakin zuen Jaialdiko zuzendariaren deiak salatu zionean Martín Belzunce, best seller-en idazle ezaguna, desagertua zela arrastorik utzi gabe. \n\n" +
                    "Eduki lazgarriaren ondoan, USB memoria bat zegoen. Inspektoreak ordenagailuan sartu zuen eta PLAY sakatu zuen…";
        }

    }



    public String devolverIntroduccion2Activity(){




        if (this.idioma == true){

            return "INT. SÓTANO \n" +
                    "Un encapuchado. Tras él, un hombre con un saco en la cabeza, la mano vendada y manchada de sangre.\n \n "+"ENCAPUCHADO\n" +
                    "Hola, inspector., ¿Le ha gustado mi regalo? Si aun duda de la veracidad de su contenido, quiero que conozca a un viejo ammigo mio...\n \n" +
                    "ENCAPUCHADO\n" +
                    "Espero haber captado al fin toda su atencion.\n" +
                    "(pausa)\n" +
                    "Es probable que se pregunte por qué. Es muy sencillo, inspector: el señor Belzunce me ha traicionado. Hace tiemo, deposité en él mi confianza y, gracias a mí, se ha convertido en el escritor mas famoso del país. Ambos firmamos un pacto entre caballeros: yo dejaria que me convirtiera en su personaje y le proveería de contenidos, a cambio, él solo debía ser fiel a mi historia; escribir la verdad. Su ultima novela, sin embargo, no es más que una sarta de malditas mentiras. No soy nigún psicópata chapucero. Soy un artista, inspector. Y tanto usted como el señor Belzunce estan a punto de combrobarlo. \n" +
                    "(pausa)\n" +
                    "El juego es muy sencillo. Le cortaré un dedo cada hora, hasta que ya no le quede ninguno con el que escribir mas mentiras; despues, acabare, con su mísera existencia. A no ser que usted le encuentre primero. Para ello, deberá desentrañar la ubicación de cinco localizaciones y resolver un misterio en cada una de ellas.\n" +
                    "(pausa)\n" +
                    "Esta en sus manos, inspector.\n" +
                    "(pausa)\n" +
                    "¿Juega?";


        }else{
            return "INT. SÓTANO\n" +
                    "";
        }
    }


    public String devolverFinActivity(){



        if(this.idioma == true){
            return "INT.SÓTANO\n \nENCAPUCHADO\n" +
                    "Enhorabuena, inspector. Es usted mas inteligente de lo que creia. \n" +
                    "No se preocupe: soy un hobre de palabra. Encontrara al señor Belzunce encerrado en un sótano de la siguiente dirección: \n \n" +
                    "(pausa)\n" +
                    "Sin embargo, debo reconocer que no he sido del todo sincero con ningun de los dos. Me temo que no podía permitir que nuestro amigo siguiera escribiendo mas basura sobre mí. Pero no se preocupe, vivirá... \n \n" +
                    "CORTA A: \n \n" +
                    "Pñano de dedos cortados. \n \n" +
                    "CORTA A : \n \n" +
                    "ENCAPUCHADO \n" +
                    "Ahora, debo despedirme. Ha sido un placer jugar con usted, inspector. Quien sabe, quizá vuelva a tener noticias mías antes de lo que cree... \n \n"+
            "El encapuchado se acerca a la cámara y la apaga.";



        }else{
            return null;
        }

    }


}
