package com.example.zubicoa.proyecto;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class IntroduccionActivity extends Activity {


    TextView texto;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_introduccion);

       texto = (TextView) findViewById(R.id.texto);
    texto.setText((((MyApplication) this.getApplication()).devolverIntroduccionActivity().toString()));



        texto.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(IntroduccionActivity.this, VideosActivity.class);
                startActivity(intent);
            }
        });
    }
}
