package com.example.zubicoa.proyecto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class FinActivity extends AppCompatActivity {

    TextView texto;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fin);

        texto = (TextView) findViewById(R.id.texto);

        texto.setText(((MyApplication)this.getApplication()).devolverFinActivity().toString());


        texto.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {


                Intent intent = new Intent(FinActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

    }
}
