package com.example.zubicoa.proyecto;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import static com.example.zubicoa.proyecto.Introduccion2Activity.READ_BLOCK_SIZE;

public class AcertijosActivity extends Activity {

    List<String> acertijos = new ArrayList<>();
    List<String> respuestas = new ArrayList<>();
    TextView titulo;
    TextView texto;
    EditText respuesta;
    int num;
    String fichero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        acertijos =  ((MyApplication) this.getApplication()).devolverAcertijos();
        respuestas.add("distinta/otra");
        respuestas.add("");
        respuestas.add("31/12");
        respuestas.add("204862");
        respuestas.add("4/4");
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        titulo = (TextView) findViewById(R.id.titulo);




        texto = (TextView) findViewById(R.id.texto);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acertijos);



        ((MyApplication) this.getApplication()).sumarActivity();







        try {
            FileInputStream fileIn = openFileInput("nivel.txt");
            InputStreamReader InputRead = new InputStreamReader(fileIn);

            char[] inputBuffer = new char[READ_BLOCK_SIZE];
            String s = "";
            int charRead;

            while ((charRead = InputRead.read(inputBuffer)) > 0) {
                // char to string conversion
                String readstring = String.copyValueOf(inputBuffer, 0, charRead);
                s += readstring;
                num = Integer.parseInt(s);
            }
            InputRead.close();
            fichero = s;
            //Toast.makeText(getBaseContext(), s,Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
        }



        int nivel;


        if (num > 0){
            nivel = num;

            titulo = (TextView) findViewById(R.id.titulo);
            titulo.setText("Pista : " + (nivel + 1));


            texto = (TextView) findViewById(R.id.texto);
            texto.setText(acertijos.get(nivel));
            ((MyApplication) this.getApplication()).setI(nivel);
        }else{


            nivel= ((MyApplication) this.getApplication()).getI().intValue();
            titulo = (TextView) findViewById(R.id.titulo);
            titulo.setText("Acertijo : " + (nivel + 1));


            texto = (TextView) findViewById(R.id.texto);
            texto.setText(acertijos.get(nivel));
        }








        respuesta = (EditText) findViewById(R.id.respuesta);


        Button btn = (Button) findViewById(R.id.btn);

        btn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {

               // if (respuesta.equals())

                Intent intent = new Intent(AcertijosActivity.this, AciertosActivity.class);

                startActivity(intent);
            }
        });


    }
}
