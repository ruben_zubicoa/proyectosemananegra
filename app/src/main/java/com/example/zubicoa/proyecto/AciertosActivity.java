package com.example.zubicoa.proyecto;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import static com.example.zubicoa.proyecto.Introduccion2Activity.READ_BLOCK_SIZE;

public class AciertosActivity extends Activity {


    List<String> aciertos = new ArrayList<String>();
    TextView texto;
    int num;
    String fichero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        aciertos = ((MyApplication) this.getApplication()).devolverAciertos();

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);



        final int nivel = ((MyApplication) this.getApplication()).getI().intValue();


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aciertos);



        ((MyApplication) this.getApplication()).sumarActivity();











        try {
            FileOutputStream fileout=openFileOutput(    "activity.txt", MODE_PRIVATE);
            OutputStreamWriter outputWriter=new OutputStreamWriter(fileout);
            outputWriter.write( ((MyApplication) this.getApplication()).getContadorActivity());
            outputWriter.close();



        } catch (Exception e) {
            e.printStackTrace();
        }



        try {
            FileInputStream fileIn = openFileInput("nivel.txt");
            InputStreamReader InputRead = new InputStreamReader(fileIn);

            char[] inputBuffer = new char[READ_BLOCK_SIZE];
            String s = "";
            int charRead;

            while ((charRead = InputRead.read(inputBuffer)) > 0) {
                // char to string conversion
                String readstring = String.copyValueOf(inputBuffer, 0, charRead);
                s += readstring;
                num = Integer.parseInt(s);
            }
            InputRead.close();
            fichero = s;
            //Toast.makeText(getBaseContext(), s,Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
        }






        if (num > 0){





            texto = (TextView) findViewById(R.id.texto);
            texto.setText(aciertos.get(num));
            ((MyApplication) this.getApplication()).setI(nivel);
        }else{






            texto = (TextView) findViewById(R.id.texto);
            texto.setText(aciertos.get(num));
        }












       ((MyApplication) this.getApplication()).sumarI();
        ((MyApplication) this.getApplication()).sumarVideo();



        final String i = ((MyApplication) this.getApplication()).getI().toString();

        Button btn = (Button) findViewById(R.id.btn);

        btn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {


                try {
                    FileOutputStream fileout=openFileOutput("nivel.txt", MODE_PRIVATE);
                    OutputStreamWriter outputWriter=new OutputStreamWriter(fileout);
                    outputWriter.write(i);
                    outputWriter.close();



                } catch (Exception e) {
                    e.printStackTrace();
                }


                if (nivel == 4){
                    Intent intent = new Intent(AciertosActivity.this, VideosActivity.class);

                    startActivity(intent);
                }else{
                    Intent intent = new Intent(AciertosActivity.this, PistasActivity.class);

                    startActivity(intent);
                }



            }
        });


    }
}
