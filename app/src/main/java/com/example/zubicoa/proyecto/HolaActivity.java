package com.example.zubicoa.proyecto;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class HolaActivity extends Activity {


TextView texto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hola);




       Button btn = (Button) findViewById(R.id.btn);

       texto = (TextView) findViewById(R.id.texto);
       String idioma = ((MyApplication) getApplication()).devolverHolaActivity();
       texto.setText(idioma);


        btn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HolaActivity.this, IntroduccionActivity.class);
                startActivity(intent);
            }
        });


    }
}
