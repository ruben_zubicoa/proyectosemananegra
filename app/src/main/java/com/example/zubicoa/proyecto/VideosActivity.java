package com.example.zubicoa.proyecto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class VideosActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videos);

        Button btn = (Button) findViewById(R.id.btn);

        final int video = ((MyApplication) this.getApplication()).getVideo();

        btn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {


                if (video == 0){
                    Intent intent = new Intent(VideosActivity.this, Introduccion2Activity.class);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(VideosActivity.this, FinActivity.class);
                    startActivity(intent);
                }







            }
        });

    }
}
