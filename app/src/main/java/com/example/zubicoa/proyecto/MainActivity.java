package com.example.zubicoa.proyecto;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.InputStreamReader;

import static com.example.zubicoa.proyecto.Introduccion2Activity.READ_BLOCK_SIZE;


public class MainActivity extends Activity {
    TextView texto;
    String fichero;
    int num;
Button btnIdioma;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        try {
            FileInputStream fileIn = openFileInput("activity.txt");
            InputStreamReader InputRead = new InputStreamReader(fileIn);

            char[] inputBuffer = new char[READ_BLOCK_SIZE];
            String s = "";
            int charRead;

            while ((charRead = InputRead.read(inputBuffer)) > 0) {
                // char to string conversion
                String readstring = String.copyValueOf(inputBuffer, 0, charRead);
                s += readstring;
                num = Integer.parseInt(s);
            }
            InputRead.close();
            fichero = s;
            //Toast.makeText(getBaseContext(), s,Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
        }


        if (num == 1) {
            Intent intent = new Intent(MainActivity.this, PistasActivity.class);
            startActivity(intent);
        }else if(num == 2){
            Intent intent = new Intent(MainActivity.this, AcertijosActivity.class);
            startActivity(intent);
        }else if(num == 3){
            Intent intent = new Intent(MainActivity.this, AciertosActivity.class);
            startActivity(intent);
        }else{

            //  ((Idioma) this.getApplication()).cambiarIdioma();

            Button btn = (Button) findViewById(R.id.button);
            texto = (TextView) findViewById(R.id.texto);
            btnIdioma = (Button) findViewById(R.id.idioma);

            texto.setText(((MyApplication) this.getApplication()).devolverMain() + "");


            btn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MainActivity.this, HolaActivity.class);
                    startActivity(intent);
                }
            });

        }


    }



    }

